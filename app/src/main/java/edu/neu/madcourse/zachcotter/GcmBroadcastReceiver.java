package edu.neu.madcourse.zachcotter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import edu.neu.madcourse.zachcotter.bananagrams.Network;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
  @Override
  public void onReceive(Context context,
                        Intent intent) {
    if(Network.networkAvailable(context,
                                false)) {
      // Explicitly specify that GcmIntentService will handle the intent.
      ComponentName comp = new ComponentName(context.getPackageName(),
                                             GCMIntentService.class.getName());
      // Start the service, keeping the device awake while it is launching.
      startWakefulService(context,
                          (intent.setComponent(comp)));
      setResultCode(Activity.RESULT_OK);
    }
  }
}