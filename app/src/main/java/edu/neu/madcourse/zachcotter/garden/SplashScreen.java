package edu.neu.madcourse.zachcotter.garden;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import edu.neu.madcourse.zachcotter.R;


public class SplashScreen extends Activity implements OnClickListener {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.splash);
    findViewById(R.id.start_app).setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {
    switch(view.getId()){
      case R.id.start_app:
        startActivity(new Intent(this, GardenMenu.class));
        finish();
        return;
    }
  }
}
