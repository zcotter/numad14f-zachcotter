package edu.neu.madcourse.zachcotter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gcm.GCMRegistrar;

import edu.neu.madcourse.zachcotter.bananagrams.BananagramsMenu;
import edu.neu.madcourse.zachcotter.bananagrams.Communication;
import edu.neu.madcourse.zachcotter.bananagrams.Dictionary;
import edu.neu.madcourse.zachcotter.bananagrams.Network;
import edu.neu.madcourse.zachcotter.garden.MapActivity;
import edu.neu.madcourse.zachcotter.garden.SplashScreen;
import edu.neu.madcourse.zachcotter.sudoku.Sudoku;

public class MainMenu extends Activity implements OnClickListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_menu);

    this.setTitle(R.string.app_name);
    GCMRegistrar.checkDevice(this);
    GCMRegistrar.checkManifest(this);
    GCMRegistrar.register(this,
                          Network.GCM_SENDER_ID);
    findViewById(R.id.about_button).setOnClickListener(this);
    findViewById(R.id.generate_error_button).setOnClickListener(this);
    findViewById(R.id.quit_button).setOnClickListener(this);
    findViewById(R.id.sudoku_button).setOnClickListener(this);
    findViewById(R.id.dictionary_button).setOnClickListener(this);
    findViewById(R.id.word_game_button).setOnClickListener(this);
    findViewById(R.id.communication_button).setOnClickListener(this);
    findViewById(R.id.two_player_button).setOnClickListener(this);
    findViewById(R.id.tricky_button).setOnClickListener(this);
    findViewById(R.id.final_button).setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {
    switch(view.getId()) {
      case R.id.about_button:
        startActivity(new Intent(this,
                                 About.class));
        finish();
        return;
      case R.id.generate_error_button:
        throw new RuntimeException();
      case R.id.quit_button:
        finish();
        return;
      case R.id.sudoku_button:
        startActivity(new Intent(this,
                                 Sudoku.class));
        return;
      case R.id.dictionary_button:
        startActivity(new Intent(this,
                                 Dictionary.class));
        finish();
        return;
      case R.id.word_game_button:
      case R.id.two_player_button:
        startActivity(new Intent(this,
                                 BananagramsMenu.class));
        finish();
        return;
      case R.id.communication_button:
        startActivity(new Intent(this,
                                 Communication.class));
        finish();
        return;
      case R.id.tricky_button:
        startActivity(new Intent(this,
                                 MapActivity.class));
        finish();
        return;
      case R.id.final_button:
        startActivity(new Intent(this,
                                 SplashScreen.class));
        finish();
        return;
    }
  }
}
