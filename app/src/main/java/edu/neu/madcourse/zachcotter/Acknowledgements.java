package edu.neu.madcourse.zachcotter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

public abstract class Acknowledgements {

  protected abstract int getAcknowledgements();

  public Dialog createDialog(Activity a) {
    AlertDialog.Builder builder = new AlertDialog.Builder(a);
    builder.setMessage(getAcknowledgements())
      .setCancelable(false)
      .setTitle(R.id.acknowledgements_title)
      .setPositiveButton(R.id.acknowledgements_dismissal_text,
                         new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog,
                                               int id) {
                             // go away
                           }
                         });
    return builder.create();
  }
}
