package edu.neu.madcourse.zachcotter.garden;

public interface JSONSerializable {
  public String serialize();
  public void deserialize(String json);
}
