package edu.neu.madcourse.zachcotter.bananagrams;

import edu.neu.madcourse.zachcotter.Acknowledgements;
import edu.neu.madcourse.zachcotter.R;

public class BananagramsAcknowledgements extends Acknowledgements {
  @Override
  protected int getAcknowledgements() {
    return R.id.bananagrams_acknowledgements_text;
  }
}
