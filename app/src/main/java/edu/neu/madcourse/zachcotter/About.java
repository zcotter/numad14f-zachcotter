package edu.neu.madcourse.zachcotter;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.widget.TextView;

public class About extends Activity {
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.about);

    ActionBar actionBar = getActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);

    //Add the device ID as text to the imei TextView
    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    ((TextView) findViewById(R.id.imei)).setText(telephonyManager.getDeviceId());
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    // Go to the main menu when back button selected
    startActivity(new Intent(this,
                             MainMenu.class));
    finish();
    return true;
  }
}
