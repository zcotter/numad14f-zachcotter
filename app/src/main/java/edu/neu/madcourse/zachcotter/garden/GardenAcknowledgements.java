package edu.neu.madcourse.zachcotter.garden;

import edu.neu.madcourse.zachcotter.Acknowledgements;
import edu.neu.madcourse.zachcotter.R;

/**
 * Created by zcotter on 12/6/14.
 */
public class GardenAcknowledgements extends Acknowledgements {
  @Override
  protected int getAcknowledgements() {
    return R.string.garden_acknowledgements;
  }
}
