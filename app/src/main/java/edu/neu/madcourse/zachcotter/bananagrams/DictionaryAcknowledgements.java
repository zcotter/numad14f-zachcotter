package edu.neu.madcourse.zachcotter.bananagrams;

import edu.neu.madcourse.zachcotter.Acknowledgements;
import edu.neu.madcourse.zachcotter.R;

public class DictionaryAcknowledgements extends Acknowledgements {
  @Override
  protected int getAcknowledgements() {
    return R.id.dictionary_acknowledgements_text;
  }
}
