# preprocesses alphabetical wordlist into filter index
# used ruby because Java is fat and ugly

# to run:
# ruby preprocess_dictionary.rb PATH_TO_WORDLIST 3


path = ARGV[0]
depth = ARGV[1].to_i

OUTFILE_PREFIX = 'wordlist_filter_'
OUTFILE_TYPE = '.txt'

depth.downto(1).each do |current_depth|
  phrase_list = File.open(path, 'r').read
  File.open("wordlist_filter_#{current_depth}.txt", 'w') { |outfile|
    (('a' * current_depth)..('z' * current_depth)).each do |prefix|
      File.open(path, 'r').each_line do |line|
        if line[0..(current_depth - 1)] == prefix
          index = phrase_list.index("\n#{line}") || 0
          outfile.write("#{prefix}:#{index}\n")
          break
        end
      end
    end
  }
  path = "#{OUTFILE_PREFIX}#{current_depth}#{OUTFILE_TYPE}"
end
